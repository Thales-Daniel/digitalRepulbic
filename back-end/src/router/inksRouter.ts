import express from 'express';
import inkCalculatorController from '../controller/inks';

const router = express.Router({ mergeParams: true });

router.post('/', inkCalculatorController);

export default router;
