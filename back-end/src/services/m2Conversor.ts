import { IWallType } from '../types';

function m2Conversor(measurements : IWallType[]) {
  let wallsAcumulator = 0;
  let windowsAndDoorsAcumulator = 0;
  for (let index = 0; index < measurements.length; index += 1) {
    const wallSum = measurements[index].width * measurements[index].height;
    const windows = measurements[index].windows * 2.0 * 1.20;
    const doors = measurements[index].doors * 0.80 * 1.90;
    const windowsAndDoors = windows + doors;
    wallsAcumulator += wallSum;
    windowsAndDoorsAcumulator += windowsAndDoors;
  }

  const manyWindowsAndDoors = 'many windows and doors, the area of the windows and doors is more than 50% of the area of the walls';

  if (windowsAndDoorsAcumulator > wallsAcumulator / 2) {
    return { error: manyWindowsAndDoors };
  }

  wallsAcumulator -= windowsAndDoorsAcumulator;

  return wallsAcumulator;
}

export default m2Conversor;
