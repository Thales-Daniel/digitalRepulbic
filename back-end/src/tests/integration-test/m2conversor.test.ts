import request from 'supertest';
import app from '../../api/app';

import {
  measurements,
  measumentsErrorFields,
  measurementsErrorTooSmall,
  measurementsErrorTooBig,
  measurementsErrorHeight,
} from '../mocks/inks';

describe('Rota Post /m2Conversor', () => {
  describe('quando os dados do body estão validos', () => {
    it('Deve retornar um objeto contendo uma tag area com o valor da area calculada', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measurements);

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({ area: 19.68 });
    });
  });

  describe('quando os dados do body são invalidos', () => {
    it('Quando a area é menor ou igual a 1m²', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measurementsErrorTooSmall);

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    });

    it('Quando a area é maior que 50m²', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measurementsErrorTooBig);

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    });

    it('Quando a area é maior que 50m²', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measurementsErrorTooBig);

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    });

    it('Quando o body está vazio', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send();

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'Measurements are required' });
    });

    it('Quando esta faltando algum campo', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measumentsErrorFields);

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'all fields are required' });
    });

    it('Quando a altura não é de 30 centimetros a mais que a porta', async () => {
      const req = request(app);
      const response = await req.post('/m2Conversor')
        .set('Content-type', 'application/json')
        .send(measurementsErrorHeight);

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'walls with doors need to be 30 centimeters higher than the doors' });
    });
  });
});
