import request from 'supertest';
import app from '../../api/app';

import { inkCalculated } from '../mocks/inks';

describe('Rota Post /inkCalculator', () => {
  describe('quando os dados do body estão validos', () => {
    it('Deve retornar um objeto contendo uma string com as tintas e outro objeto com tintas', async () => {
      const req = request(app);
      const response = await req.post('/inkCalculator')
        .set('Content-type', 'application/json')
        .send({ area: 50 });

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual(inkCalculated);
    });
  });

  describe('quando os dados do body são invalidos', () => {
    it('Quando a area é menor ou igual a 1m²', async () => {
      const req = request(app);
      const response = await req.post('/inkCalculator')
        .set('Content-type', 'application/json')
        .send({ area: 1 });

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    });

    it('Quando a area é maior que 50m²', async () => {
      const req = request(app);
      const response = await req.post('/inkCalculator')
        .set('Content-type', 'application/json')
        .send({ area: 51 });

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    });

    it('Quando a area não é enviada', async () => {
      const req = request(app);
      const response = await req.post('/inkCalculator')
        .set('Content-type', 'application/json')
        .send();

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'Area is required' });
    });

    it('Quando a area é uma string', async () => {
      const req = request(app);
      const response = await req.post('/inkCalculator')
        .set('Content-type', 'application/json')
        .send({ area: '50' });

      expect(response.statusCode).toBe(400);
      expect(response.body).toEqual({ error: 'area must be a number' });
    });
  });
});
