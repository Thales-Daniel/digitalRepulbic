export const inkCalculated = {
  stringWithInks: 'You need a total of 4 cans: 1 of 2.5 Liters, 2 of 3.6 Liters and 1 of 0.5 Liters',
  inks: {
    3.6: 2,
    2.5: 1,
    0.5: 1,
  },
};

export const measurements = {
  measurements: [
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '2',
    },
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '1',
    },
    {
      height: '4',
      doors: '0',
      windows: '1',
      width: '2',
    },
    {
      height: '3',
      doors: '1',
      windows: '1',
      width: '3',
    },
  ],
};

export const measumentsErrorFields = {
  measurements: [
    {
      doors: '0',
      windows: '0',
      width: '2',
    },
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '1',
    },
    {
      height: '4',
      doors: '0',
      windows: '1',
      width: '2',
    },
    {
      height: '3',
      doors: '1',
      windows: '1',
      width: '3',
    },
  ],
};

export const measurementsErrorTooSmall = {
  measurements: [
    {
      height: '0',
      doors: '0',
      windows: '0',
      width: '0',
    },
    {
      height: '0',
      doors: '0',
      windows: '0',
      width: '0',
    },
    {
      height: '0',
      doors: '0',
      windows: '1',
      width: '0',
    },
    {
      height: '0',
      doors: '0',
      windows: '0',
      width: '0',
    },
  ],
};

export const measurementsErrorTooBig = {
  measurements: [
    {
      height: '10',
      doors: '0',
      windows: '0',
      width: '5',
    },
    {
      height: '10',
      doors: '0',
      windows: '0',
      width: '5',
    },
    {
      height: '10',
      doors: '0',
      windows: '1',
      width: '5',
    },
    {
      height: '10',
      doors: '0',
      windows: '0',
      width: '5',
    },
  ],
};

export const measurementsErrorHeight = {
  measurements: [
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '2',
    },
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '3',
    },
    {
      height: '2',
      doors: '1',
      windows: '0',
      width: '2',
    },
    {
      height: '3',
      doors: '0',
      windows: '0',
      width: '3',
    },
  ],
};
