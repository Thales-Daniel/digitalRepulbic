import { StatusCodes } from 'http-status-codes';
import { Handler } from '../types';

const areaValidator : Handler = (req, res, next) => {
  try {
    const { area } = req.body;

    if (area <= 1 || area > 50) {
      return res.status(StatusCodes.BAD_REQUEST)
        .json({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
    }

    if (!area) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: 'Area is required' });
    }

    if (typeof area !== 'number') {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: 'area must be a number' });
    }

    next();
  } catch (e) {
    next(e);
  }
};

export default areaValidator;
