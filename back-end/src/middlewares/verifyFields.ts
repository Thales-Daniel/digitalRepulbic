import { StatusCodes } from 'http-status-codes';
import { Handler } from '../types';

const verifyFields : Handler = (req, res, next) => {
  const { measurements } = req.body;
  let area = 0;
  let windowsAndDoors = 0;
  if (!measurements) {
    return res.status(StatusCodes.BAD_REQUEST).json({ error: 'Measurements are required' });
  }

  for (let index = 0; index < measurements.length; index += 1) {
    const windowsArea = measurements[index].windows * 2.0 * 1.20;
    const doorsArea = measurements[index].doors * 0.80 * 1.90;
    const {
      height, width, doors, windows,
    } = measurements[index];

    if (height === '' || width === '' || doors === '' || windows === '') {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: 'All fields are required' });
    }

    if (!height || !width || !doors || !windows) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: 'all fields are required' });
    }

    if (doors > 0 && height < 2.20) {
      return res.status(StatusCodes.BAD_REQUEST).json({ error: 'walls with doors need to be 30 centimeters higher than the doors' });
    }

    windowsAndDoors += windowsArea + doorsArea;
    area += height * width;
  }

  area -= windowsAndDoors;

  if (area <= 1 || area > 50) {
    return res.status(StatusCodes.BAD_REQUEST)
      .json({ error: 'any wall must be more than 1m² and be less than or equal to 50m²' });
  }

  next();
};

export default verifyFields;
