import React, {
  createContext, useState, useMemo, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import m2Conversor from '../functions/m2Conversor';
import getInks from '../functions/getInks';

export const WallsContext = createContext();

function WallsProvider(props) {
  const [wallOne, setWallOne] = useState({});
  const [wallTwo, setWallTwo] = useState({});
  const [wallThree, setWallThree] = useState({});
  const [wallFour, setWallFour] = useState({});
  const [walls, setWalls] = useState([]);
  const [verify, setVerify] = useState(false);
  const [verifyTwo, setVerifyTwo] = useState(false);
  const [verifyThree, setVerifyThree] = useState(false);
  const [verifyFour, setVerifyFour] = useState(false);
  const [verifyAll, setVerifyAll] = useState(false);
  const [calculeOrError, setCalculeOrError] = useState('');

  useEffect(() => {
    if (verify && verifyTwo && verifyThree && verifyFour) {
      setVerifyAll(true);
    }
  }, [verify, verifyTwo, verifyThree, verifyFour]);

  useEffect(() => {
    setWalls([wallOne, wallTwo, wallThree, wallFour]);
  }, [wallOne, wallTwo, wallThree, wallFour]);

  const onSubmit = async (event) => {
    event.preventDefault();
    setWalls([wallOne, wallTwo, wallThree, wallFour]);
    const m2Converted = await m2Conversor(walls);
    if (m2Converted.error) {
      setCalculeOrError(m2Converted.error);
    } else {
      const getInk = await getInks(m2Converted.area);
      setCalculeOrError(getInk.stringWithInks);
    }
  };

  const context = {
    wallOne,
    setWallOne,
    wallTwo,
    setWallTwo,
    wallThree,
    setWallThree,
    wallFour,
    setWallFour,
    onSubmit,
    walls,
    verify,
    setVerify,
    verifyTwo,
    setVerifyTwo,
    verifyThree,
    setVerifyThree,
    verifyFour,
    setVerifyFour,
    verifyAll,
    calculeOrError,
  };

  const contextValue = useMemo(() => (context), [context]);

  const { children } = props;

  return (
    <WallsContext.Provider value={contextValue}>
      {children}
    </WallsContext.Provider>
  );
}

WallsProvider.propTypes = {
  children: PropTypes.object,
}.isRequired;

export default WallsProvider;
