import React, { useContext } from 'react';
import Wall from '../wall/Wall';
import { WallsContext } from '../../context/WallProvider';
import './style.css';

function Form() {
  const {
    setWallOne,
    setWallTwo,
    setWallThree,
    setWallFour,
    setVerify,
    setVerifyTwo,
    setVerifyThree,
    setVerifyFour,
    onSubmit,
    verifyAll,
    calculeOrError,
  } = useContext(WallsContext);

  return (
    <div className="formFather">
      <form className="formContainer" onSubmit={onSubmit}>
        <div className="separatorFather">
          <div className="wallSeparator">
            <Wall setMeasurements={setWallOne} setVerify={setVerify} name="Wall 1" />
            <Wall setMeasurements={setWallTwo} setVerify={setVerifyTwo} name="Wall 2" />
          </div>
          <div className="wallSeparator">
            <Wall setMeasurements={setWallFour} setVerify={setVerifyFour} name="Wall 4" />
            <Wall setMeasurements={setWallThree} setVerify={setVerifyThree} name="Wall 3" />
          </div>
        </div>
        <h1>{calculeOrError}</h1>
        <button type="submit" disabled={!verifyAll} className="submitButton">GENERATE</button>
      </form>
    </div>
  );
}

export default Form;
