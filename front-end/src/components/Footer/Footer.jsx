import React from 'react';
import './style.css';

function Footer() {
  return (
    <header className="footer">
      <p>&copy; Created by Thales Daniel</p>
    </header>
  );
}

export default Footer;
