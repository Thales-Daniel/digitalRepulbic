# Boas vindas ao repositório Ink Calculator!


Este repositório tem como objetivo criar um aplicação Full-stack, onde sera criado Uma aplicação web ou mobile que ajude o usuário a calcular a quantidade de tinta necessária para pintar uma sala.
Essa aplicação deve considerar que a sala é composta de 4 paredes e deve permitir que o usuário escolha qual a medida de cada parede e quantas janelas e portas possuem cada parede.
Com base na quantidade necessária o sistema deve apontar tamanhos de lata de tinta que o usuário deve comprar, sempre priorizando as latas maiores. Ex: se o usuário precisa de 19 litros, ele deve sugerir 1 lata de 18L + 2 latas de 0,5L


# Sumário
- [Instruções](#instruções)
- [Tecnologias](#tecnologias)
- [Executando o projeto](#executando-o-projeto)
- [Proximos passos](#proximos-passos)
- [Principal Desafio](#desafio-principal)
- [Testes](#testes)


# Instruções:

Inicie clonando o repositorio para sua máquina local 
~~~
git clone git@gitlab.com:Thales-Daniel/InkCalculator.git
~~~
entre na pasta do repositorio
~~~
cd InkCalculator
~~~
Logo em seguida, Utilize o comando npm run buildAll, 
pode demorar um pouca ja que instala as dependencias de todas as aplicações
~~~
npm run buildAll
~~~
É necessario também entrar na pasta do back-end e criar o arquivo
.env, assim como o .envExample.

E para finalmente iniciar o projeto, é necessário o comando a baixo
~~~
npm start
~~~

# Tecnologias

O front-end do projeto foi desenvolvido em React.js com o gerenciador de estado Context-api, foram utilizadas
4 bibliotecas para auxiliar o desenvolvimento do projeto.


  - `Axios`
  - `eslint`
  - `React-router-dom`
  - `React-testing-library`

<div align="center">
  <img alt="eslint" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/eslint/eslint-original.svg" />
  <img alt="axios" height="60" width="80" src="https://upload.wikimedia.org/wikipedia/commons/c/c8/Axios_logo_%282020%29.svg" />
  <img alt="router-dom" height="90" width="100" src="https://testing-library.com/img/octopus-128x128.png" />
  <br />
  <br />
</div>
  <br />
  <br />
  
  O back-end do projeto foi desenvolvido em Node.js com Express e Typescript. As tecnologias para desenvolver a API foram as seguintes:

  - `Node.js`
  - `Express`
  - `Eslint`
  - `Jest`
  - `Typescript`
  - `Super Test`
  - `Dotenv`
  - `Http Status Codes`


<div align="center">
  <img alt="Node" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/nodejs/nodejs-original.svg" />
  <img alt="express" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/express/express-original.svg" />
  <img alt="eslint" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/eslint/eslint-original.svg" />
  <img alt="jest" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/jest/jest-plain.svg" />
  <img alt="typescript" height="60" width="80" src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/typescript/typescript-original.svg" />
  <br />
  <br />
</div>
  <br />
  <br />


# Executando o projeto

  Front-end

  ![front](/uploads/f34530acbaf9e52dd96534b9870f91f2/front.gif)

  <div align="center">
  <img  src="back-end/src/assets/inkCalculator.png" />
  <img  src="back-end/src/assets/m2Conversor.png" />
  <br />
  <br />
</div>


# Testes
  Front-end

  <strong>Os testes ainda estão em desenvolvimento</strong>

  Back-end

  Os testes de integração foram desenvolvidos com jest e supertests.
  <div>
    <img src="back-end/src/assets/testes.png" />
  <div>
  

# Desafio Principal
  
O principal desafio do projeto foi organizar por onde seria iniciado a produção, alem disso, houveram problemas na hora da estilização, que
causou um atraso grande.



# Proximos passos

O proximo passo seria dar prosseguimento aos testes e alem disso
componentizar tudo que é possível da parte de front-end projeto.
